#ifndef _ACTIVATION_FUNC_H_

#define _ACTIVATION_FUNC_H_

#include <math.h>

double activation_function(const double arg);

#endif