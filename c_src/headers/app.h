#ifndef _APP_H_

#define _APP_H_

#include <stdio.h>
#include "errors.h"
#include "create.h"
#include "activation_func.h"
#include "params.h"
#include "free.h"
#include "parser.h"
#include "train.h"
#include "query.h"
#include "save.h"

#endif