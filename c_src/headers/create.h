#ifndef _NEURAL_NETWORK_H_

#define _NEURAL_NETWORK_H_

#include <stdlib.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include "struct.h"
#include "errors.h"

#define TIMES_TO_GET_NORMAL 6

network_t *create_network(const int in_nodes, const int hidden_nodes, const int out_nodes,\
                          const float learn_rate,\
                          double (*activaton_func)(const double));
double **create_weights(const int rows, const int cols);

#endif