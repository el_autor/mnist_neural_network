#ifndef _ERRORS_H_

#define _ERRORS_H_

#include "stdio.h"

// Коды ошибок программы
#define OK 0
#define ERROR 1
#define MEMORY_ERROR 100
#define FILE_NOT_OPENED 101

int show_error(FILE *const stream, const int code);

#endif