#ifndef _FREE_H_

#define _FREE_H_

#include <stdlib.h>
#include "struct.h"

void free_all(network_t *network);
void free_data(data_t *data);
void free_weights(double **weights, const int rows);

#endif