#ifndef _MATRIX_FRAMEWORK_H_

#define _MATRIX_FRAMEWORK_H_

#include <stdint.h>
#include <stdlib.h>

#define INT 0
#define DOUBLE 1

double **transpose(double **matrix, const int rows, const int cols);
double **multiply(double **matrix_1, double **matrix_2, const int rows_1, const int cols_1, const int cols_2);
double **activate(double **matrix, const int rows, const int cols, double (*func)(double));
double **addition(double **matrix_dest, double **matrix_src, const int rows, const int cols, const int key);
void get_result_after_output(double **matrix, const int cols, const int rows);
void straight_multiply(double **matrix_1, double **matrix_2, const int rows, const int cols);
void mult_by_number(const double multiplier, double **matrix, const int rows, const int cols);

#endif