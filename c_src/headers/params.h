#ifndef _PARAMS_H_

#define _PARAMS_H_

#define INPUT_NODES 784
#define HIDDEN_NODES 200
#define OUTPUT_NODES 10
#define LEARNING_RATE 0.1

#endif