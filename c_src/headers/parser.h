#ifndef _PARSER_H_

#define _PARSER_H_

#include <stdlib.h>
#include <errno.h>
#include "struct.h"
#include "errors.h"

#define TOTAL_PIXELS 784

int parse_data(const char *const filename, data_t *const total_data);

#endif