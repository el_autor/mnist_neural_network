#ifndef _QUERY_H_

#define _QUERY_H_

#include <stdio.h>
#include <stdlib.h>
#include "train.h"
#include "params.h"
#include "struct.h"
#include "errors.h"

uint8_t *query(network_t *const network, const data_t *const data);

#endif 
