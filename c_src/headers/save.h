#ifndef _SAVE_H_

#define _SAVE_H_

#include <stdio.h>
#include <errno.h>
#include "params.h"
#include "struct.h"
#include "errors.h"

#define WEIGHTS_FILE "./weights/weights.txt"

int save_weights(const network_t *const network, const char *const filename);

#endif