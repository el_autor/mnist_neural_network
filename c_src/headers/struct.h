#ifndef _STRUCT_H_

#define _STRUCT_H_

#include <stdint.h>

typedef struct
{
    // Кол-во узлов по слоям
    int i_nodes;
    int h_nodes;
    int o_nodes;

    // Коэффициент обучения
    float learning_rate;

    // Веса между слоями
    double **i_h_weights;
    double **h_o_weights;

    // Функция активации
    double (*activation_func)(double);

} network_t;

typedef struct
{
    uint8_t **data;
    size_t inputed_size;
    size_t alloced_size;
} data_t;

#endif