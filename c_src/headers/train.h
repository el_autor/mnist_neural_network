#ifndef _TRAIN_H_

#define _TRAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include "struct.h"
#include "params.h"
#include "free.h"
#include "matrix_framework.h"

void train(network_t *const netwotk, const data_t *const data);
void get_result_vector(double *const result_vector, uint8_t picture_answer, const int vector_size);
void get_input_vector(double *const input_vector, uint8_t *start_input_vector, const int vector_size);

#endif