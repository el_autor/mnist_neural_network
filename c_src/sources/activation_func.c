#include "../headers/activation_func.h"

double activation_function(const double arg)
{
    return (1 / (1 + exp(-arg)));
}