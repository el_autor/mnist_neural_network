#include "../headers/app.h"

#include <time.h>

int main(int argc, char **argv)
{
    network_t *network = NULL;
    data_t train_data = { NULL, 0, 0, }, query_data = { NULL, 0, 0 }; 
    clock_t start = 0, end = 0;
    uint8_t *network_results_vector = NULL;

    start = clock();
    // network init
    if ((network = create_network(INPUT_NODES, HIDDEN_NODES, OUTPUT_NODES, LEARNING_RATE, &activation_function)) == NULL)
    {
        return show_error(stdout, errno);
    }

    if (parse_data(argv[1], &train_data) != OK)
    {
        free_all(network);
        return show_error(stdout, errno);
    }

    if (parse_data(argv[2], &query_data) != OK)
    {
        free_all(network);
        free_data(&train_data);
        return show_error(stdout, errno);
    }

    // network train
    train(network, &train_data);    

    // query_network
    network_results_vector = query(network, &query_data);

    if (save_weights(network, WEIGHTS_FILE) != OK)
    {
        show_error(stdout, errno);
    } 

    free_all(network);
    free_data(&train_data);
    free_data(&query_data);
    free(network_results_vector);

    end = clock();
    printf("total_time %lf\n", (double)(end - start) / CLOCKS_PER_SEC);

    return OK;
}
