#include "../../headers/app.h"

int main(int argc, char **argv)
{
    network_t *network = NULL;
    data_t train_data = { NULL, 0, 0 };

    if ((network = create_network(INPUT_NODES, HIDDEN_NODES, OUTPUT_NODES, LEARNING_RATE, &activation_function)) == NULL)
    {
        return show_error(stdout, errno);
    }

    if (parse_data(argv[1], &train_data) != OK)
    {
        free_all(network);
        return show_error(stdout, errno);
    }

    // network train
    train(network, &train_data);    

    if (save_weights(network, WEIGHTS_FILE) != OK)
    {
        show_error(stdout, errno);
    }

    printf("Learing finished!\n"); 

    return OK;
}