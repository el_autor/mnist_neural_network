#include "../../headers/app.h"

#define SAVE_RESULT_FILE "./sources/bot_src/user_photo_data/number.txt"

int get_weights(network_t *const network, const char *const filepath)
{
    FILE *stream = NULL;

    if ((stream = fopen(filepath, "r")) == NULL)
    {
        errno = FILE_NOT_OPENED;
        return ERROR;
    }

    for (int i = 0; i < HIDDEN_NODES; i++)
    {
        for (int j = 0; j < INPUT_NODES; j++)
        {
            fscanf(stream, "%lf", &(network->i_h_weights[i][j]));
        }
    }

    for (int i = 0; i < OUTPUT_NODES; i++)
    {
        for (int j = 0; j < HIDDEN_NODES; j++)
        {
            fscanf(stream, "%lf", &(network->h_o_weights[i][j]));
        }
    }

    fclose(stream);

    return OK;
}

int save_result(const char *const filepath, const uint8_t number)
{
    FILE *stream = NULL;

    if ((stream = fopen(filepath, "w")) == NULL)
    {
        errno = FILE_NOT_OPENED;
        return ERROR;
    }

    fprintf(stream, "%hi", number);
    fclose(stream);

    return OK;
}

int main(int argc, char **argv)
{
    network_t *network = NULL;
    data_t query_data = { NULL, 0, 0 };
    uint8_t *result_numbers = NULL;

    if ((network = create_network(INPUT_NODES, HIDDEN_NODES, OUTPUT_NODES, LEARNING_RATE, &activation_function)) == NULL)
    {
        return show_error(stdout, errno);
    }

    if (parse_data(argv[1], &query_data) != OK)
    {
        free_all(network);
        return show_error(stdout, errno);
    }

    if (get_weights(network, WEIGHTS_FILE) != OK)
    {
        return show_error(stdout, errno);
    }

    result_numbers = query(network, &query_data);

    if (save_result(SAVE_RESULT_FILE, result_numbers[0]) != OK)
    {
        return show_error(stdout, errno);
    }

    printf("Number is %hi\n", result_numbers[0]);
    free(result_numbers);

    return OK;
}