#include "../headers/create.h"

// Нормальное  распределение
static double generate_weight(const double centre, const double deviation)
{
    int int_deviation = (int) (1000000 * deviation);
    int int_centre = (int) (1000000 * centre);
    int value = 0;

    for (int i = 0; i < TIMES_TO_GET_NORMAL; i++)
    {
        value += (int_centre + rand() % ((int) (2 * int_deviation))) - int_deviation;
    }
    
    value /= TIMES_TO_GET_NORMAL;

    return value / (double) 1000000;
}

static void fill_weights(double **weights, const int rows, const int cols)
{
    double deviation = 1 / sqrt(rows);

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            weights[i][j] = generate_weight(0, deviation);
        }
    }
}

double **create_weights(const int rows, const int cols)
{
    double **weights = NULL;
    srand(time(NULL));

    if ((weights = malloc(sizeof(double *) * rows)) == NULL)
    {
        errno = MEMORY_ERROR;
        return NULL;
    }

    for (int i = 0; i < rows; i++)
    {
        if ((*(weights + i) = malloc(sizeof(double) * cols)) == NULL)
        {
            errno = MEMORY_ERROR;
            return NULL;
        }
    }

    fill_weights(weights, rows, cols);

    return weights;
}

network_t *create_network(const int in_nodes, const int hidden_nodes, const int out_nodes,\
                          const float learn_rate,\
                          double (*activaton_func)(const double))
{
    network_t *network = NULL;

    if ((network = malloc(sizeof(network_t))) == NULL)
    {
        errno = MEMORY_ERROR;
        return NULL;
    }

    network->i_nodes = in_nodes;
    network->h_nodes = hidden_nodes;
    network->o_nodes = out_nodes;
    network->learning_rate = learn_rate;

    if ((network->i_h_weights = create_weights(hidden_nodes, in_nodes)) == NULL)
    {
        return NULL;
    }

    if ((network->h_o_weights = create_weights(out_nodes, hidden_nodes)) == NULL)
    {
        return NULL;
    }

    network->activation_func = activaton_func;

    return network;
}