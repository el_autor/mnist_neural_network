#include "../headers/errors.h"

int show_error(FILE *const stream, const int code)
{
    switch (code)
    {
        case MEMORY_ERROR:
            fprintf(stream, "Memory not alloced!\n");
            break;
    } 

    return code;
}

