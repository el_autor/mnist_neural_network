#include "../headers/free.h"

void free_weights(double **weights, const int rows)
{
    for (int i = 0; i < rows; i++)
    {
        free(weights[i]);
    }

    free(weights);
}

void free_all(network_t *network)
{
    free_weights(network->i_h_weights, network->h_nodes);
    free_weights(network->h_o_weights, network->o_nodes);
    free(network);
}

void free_data(data_t *data)
{
    for (int i = 0; i < data->inputed_size; i++)
    {
        free(data->data[i]);
    }

    free(data->data);
}