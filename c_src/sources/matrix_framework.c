#include "../headers/matrix_framework.h"

//#include <stdio.h>
//#include <time.h>

double **transpose(double **matrix, const int rows, const int cols)
{
    double **new_matrix = NULL;

    //clock_t start, end;

    //start = clock();
    new_matrix = malloc(sizeof(double *) * cols);

    for (int i = 0; i < cols; i++)
    {
        new_matrix[i] = malloc(sizeof(double) * rows);
    }
    //end = clock();
    //printf("allocate memory - %lf\n", (double)(end -start) / CLOCKS_PER_SEC);

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            new_matrix[j][i] = matrix[i][j];
        }
    }

    return new_matrix;
}

double **multiply(double **matrix_1, double **matrix_2, const int rows_1, const int cols_1, const int cols_2)
{
    double **result_matrix = NULL;
    //clock_t start, end;

    //start = clock();
    result_matrix = malloc(sizeof(double *) * rows_1);

    
    for (int i = 0; i < rows_1; i++)
    {
        result_matrix[i] = malloc(sizeof(double) * cols_2);

        for (int j = 0; j < cols_2; j++)
        {
            result_matrix[i][j] = 0;
        }
    }

    //end = clock();

    //printf("allocate multiply - %lf\n", (double)(end -start) / CLOCKS_PER_SEC);
    
    //start = clock();
    /*
    for (double **i = matrix_1; i < matrix_1 + rows_1; i++)
    {
        for (int j = 0; j < cols_2; j++)
        {
            for (double *k = *i; k < *i + cols_1; k++)
            {
                result_matrix[i - matrix_1][j] = *k * matrix_2[k - *i][j];  
            }
        }
    }
    */

    for (int i = 0; i < rows_1; i++)
    {
        for (int j = 0; j < cols_2; j++)
        {
            for (int k = 0; k < cols_1; k++)
            {
                result_matrix[i][j] += matrix_1[i][k] * matrix_2[k][j]; 
            }
        }
    }
    
    //end = clock();
    //printf("multiply time - %lf\n", (double)(end -start) / CLOCKS_PER_SEC);

    return result_matrix;
}

double **activate(double **matrix, const int rows, const int cols, double (*func)(double))
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix[i][j] = func(matrix[i][j]);
        }
    }

    return matrix;
}

double **addition(double **matrix_dest, double **matrix_src, const int rows, const int cols, const int key)
{
    
    for (double **i_1 = matrix_dest, **i_2 = matrix_src; i_1 < matrix_dest + rows; i_1++, i_2++)
    {
        for (double *j_1 = *i_1, *j_2 = *i_2; j_1 < *i_1 + cols; j_1++, j_2++)
        {
            (*j_1) += (*j_2) * key;
        }
    }
    
    /*
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix_dest[i][j] += matrix_src[i][j] * key;
        }
    }
    */

    return matrix_dest;
}

void get_result_after_output(double **matrix, const int rows, const int cols)
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix[i][j] *= (1.0 - matrix[i][j]);
        }
    }
}

void straight_multiply(double **matrix_1, double **matrix_2, const int rows, const int cols)
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix_1[i][j] *= matrix_2[i][j]; 
        }
    }
}

void mult_by_number(const double multiplier, double **matrix, const int rows, const int cols)
{
    for (double **i = matrix; i < matrix + rows; i++)
    {
        for (double *j = *i; j < *i + cols; j++)
        {
            (*j) *= multiplier;
        }
    }

    /*
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix[i][j] *= multiplier;
        }
    }*/  
}