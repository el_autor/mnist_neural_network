#include "../headers/parser.h"

static uint8_t *get_example(FILE *const stream, const int result_number)
{
    uint8_t *example = NULL;

    example = malloc(sizeof(uint8_t) * (TOTAL_PIXELS + 1));
    example[0] = result_number;

    for (int i = 1; i <= TOTAL_PIXELS; i++)
    {
        fscanf(stream, ",%hhd", example + i); 
    }

    return example; 
}

int parse_data(const char *const filename, data_t *const total_data)
{
    FILE *stream = NULL;
    uint8_t result_number = 0; // Число, нарисованное на картинке

    if ((stream = fopen(filename, "r")) == NULL)
    {
        errno = FILE_NOT_OPENED;
        return ERROR;
    }

    total_data->data = malloc(sizeof(uint8_t *));
    (total_data->alloced_size)++;

    while (fscanf(stream, "%hhd", &result_number) == 1)
    {
        if (total_data->alloced_size == total_data->inputed_size)
        {
            (total_data->alloced_size) *= 2;
            total_data->data = realloc(total_data->data, sizeof(uint8_t *) * total_data->alloced_size);
        }

        total_data->data[total_data->inputed_size] = get_example(stream, result_number);
        (total_data->inputed_size)++;
    }
    
    fclose(stream);

    return OK;
}