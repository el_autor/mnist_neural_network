#include "../headers/query.h"

static int query_by_picture(network_t *const network, double *input_vector, double *result_vector, uint8_t *const result_number)
{
    double **input_to_matrix = NULL, **result_to_matrix = NULL, **input_matrix = NULL, **result_matrix = NULL;
    double **hidden_inputs = NULL, **hidden_outputs = NULL, **final_inputs = NULL, **final_outputs = NULL;
    int max = 0, proper_max = 0;

    input_to_matrix = malloc(sizeof(double *));
    (*input_to_matrix) = input_vector;
    result_to_matrix = malloc(sizeof(double *));
    (*result_to_matrix) = result_vector;

    input_matrix = transpose(input_to_matrix, 1, INPUT_NODES);
    result_matrix = transpose(result_to_matrix, 1, OUTPUT_NODES);

    hidden_inputs = multiply(network->i_h_weights, input_matrix, HIDDEN_NODES, INPUT_NODES, 1);
    hidden_outputs = activate(hidden_inputs, HIDDEN_NODES, 1, network->activation_func);
    final_inputs = multiply(network->h_o_weights, hidden_outputs, OUTPUT_NODES, HIDDEN_NODES, 1);
    final_outputs = activate(final_inputs, OUTPUT_NODES, 1, network->activation_func);

    for (int i = 1; i < OUTPUT_NODES; i++)
    {
        if (final_outputs[i][0] > final_outputs[max][0])
        {
            max = i;
        }
    }

    for (int i = 1; i < OUTPUT_NODES; i++)
    {
        if (result_vector[i] > result_vector[proper_max])
        {
            proper_max = i;
        }
    }

    *result_number = max;
    
    free_weights(hidden_inputs, HIDDEN_NODES);
    free_weights(final_inputs, OUTPUT_NODES);
    free_weights(input_matrix, INPUT_NODES);
    free_weights(result_matrix, OUTPUT_NODES);
    free(input_to_matrix);
    free(result_to_matrix);    

    if (max != proper_max)
    {
        return ERROR;
    }

    return OK;
}

uint8_t *query(network_t *const network, const data_t *const data)
{
    double *result = malloc(sizeof(double) * OUTPUT_NODES);
    double *input = malloc(sizeof(double) * INPUT_NODES);
    int total_matches = 0;
    uint8_t *result_vector = malloc(sizeof(uint8_t) * data->inputed_size), value = 0;

    printf("%ld\n", data->inputed_size);

    for (int i = 0; i < data->inputed_size; i++)
    {
        get_result_vector(result, data->data[i][0], OUTPUT_NODES);
        get_input_vector(input, data->data[i], INPUT_NODES);

        if (query_by_picture(network, input, result, &value) == OK)
        {
            total_matches++;
        }

        result_vector[i] = value;
    }

    printf("Tolal efficiency - %lf%%\n", total_matches / (float) (data->inputed_size) * 100);
    free(result);
    free(input);

    return result_vector;
}