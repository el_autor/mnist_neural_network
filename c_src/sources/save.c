#include "../headers/save.h"

int save_weights(const network_t *const network, const char *const filename)
{
    FILE *dest = NULL;
    
    if ((dest = fopen(filename, "w")) == NULL)
    {
        errno = FILE_NOT_OPENED; 
        return ERROR;
    }

    for (int i = 0; i < HIDDEN_NODES; i++)
    {
        for (int j = 0; j < INPUT_NODES; j++)
        {
            fprintf(dest, "%lf ", network->i_h_weights[i][j]);
        }

        fprintf(dest, "\n");
    }
    
    fprintf(dest, "\n");

    for (int i = 0; i < OUTPUT_NODES; i++)
    {
        for (int j = 0; j < HIDDEN_NODES; j++)
        {
            fprintf(dest, "%lf ", network->h_o_weights[i][j]);
        }

        fprintf(dest, "\n");
    }

    fclose(dest);

    return OK;
}