#include "../headers/train.h"

//#include <time.h>

static void correct_weights(double **weights, const int rows_w, const int cols_w,\
                            const double learn_rate,\
                            double **errors, const int rows_e, const int cols_e,\
                            double **after_output, const int rows_after, const int cols_after,\
                            double **before_output, const int rows_before, const int cols_before)
{
    double **transposed_before = NULL, **result;
    //clock_t start = 0, end = 0;

    get_result_after_output(after_output, rows_after, cols_after);
    straight_multiply(errors, after_output, rows_e, cols_e);
    transposed_before = transpose(before_output, rows_before, cols_before);

    //start = clock();
    result = multiply(errors, transposed_before, rows_e, cols_e, rows_before);
    //end = clock();

    //printf("mult_time - %lf\n", (double)(end - start) / CLOCKS_PER_SEC);

    //start = clock();
    mult_by_number(learn_rate, result, rows_e, rows_before);
    //end = clock();
    //printf("mult_by_number - %lf\n", (double)(end - start) / CLOCKS_PER_SEC);

    //start = clock();
    addition(weights, result, rows_w, cols_w, 1);
    //end = clock();
    //printf("addition_time - %lf\n", (double)(end - start) / CLOCKS_PER_SEC);

    //start = clock();
    free_weights(transposed_before, cols_before);
    free_weights(result, rows_e);
    //end = clock();

    //printf("free_time - %lf\n", (double)(end - start) / CLOCKS_PER_SEC);
}

static void train_by_picture(network_t *const network, double *input_vector, double *result_vector)
{
    double **input_to_matrix = NULL, **result_to_matrix = NULL, **input_matrix = NULL, **result_matrix = NULL;
    double **hidden_inputs = NULL, **hidden_outputs = NULL, **final_inputs = NULL, **final_outputs = NULL;
    double **output_errors = NULL, **hidden_errors = NULL, **local = NULL;

    input_to_matrix = malloc(sizeof(double *));
    (*input_to_matrix) = input_vector;
    result_to_matrix = malloc(sizeof(double *));
    (*result_to_matrix) = result_vector;

    input_matrix = transpose(input_to_matrix, 1, INPUT_NODES);
    result_matrix = transpose(result_to_matrix, 1, OUTPUT_NODES);

    // Direct distribution
    hidden_inputs = multiply(network->i_h_weights, input_matrix, HIDDEN_NODES, INPUT_NODES, 1);
    hidden_outputs = activate(hidden_inputs, HIDDEN_NODES, 1, network->activation_func);
    final_inputs = multiply(network->h_o_weights, hidden_outputs, OUTPUT_NODES, HIDDEN_NODES, 1);
    final_outputs = activate(final_inputs, OUTPUT_NODES, 1, network->activation_func);

    // Back errors distibution
    output_errors = addition(result_matrix, final_outputs, OUTPUT_NODES, 1, -1);
    local = transpose(network->h_o_weights, OUTPUT_NODES, HIDDEN_NODES);
    hidden_errors = multiply(local, output_errors, HIDDEN_NODES, OUTPUT_NODES, 1);

    //clock_t start = 0, end = 0;

    //start = clock();
    correct_weights(network->h_o_weights, OUTPUT_NODES, HIDDEN_NODES,\
                    network->learning_rate,\
                    output_errors, OUTPUT_NODES, 1,\
                    final_outputs, OUTPUT_NODES, 1,\
                    hidden_outputs, HIDDEN_NODES, 1); 
    //end = clock();

    //printf("time h_o - %lf\n", (double)(end - start)/ CLOCKS_PER_SEC);

    //start = clock();
    correct_weights(network->i_h_weights, HIDDEN_NODES, INPUT_NODES,\
                    network->learning_rate,\
                    hidden_errors, HIDDEN_NODES, 1,\
                    hidden_outputs, HIDDEN_NODES, 1,\
                    input_matrix, INPUT_NODES, 1);
    //end = clock();

    //printf("time i_h - %lf\n", (double)(end - start)/ CLOCKS_PER_SEC);

    free_weights(local, HIDDEN_NODES);
    free_weights(hidden_inputs, HIDDEN_NODES);
    free_weights(final_inputs, OUTPUT_NODES);
    free_weights(input_matrix, INPUT_NODES);
    free_weights(result_matrix, OUTPUT_NODES);
    free_weights(hidden_errors, HIDDEN_NODES);
    free(input_to_matrix);
    free(result_to_matrix);
}

void get_result_vector(double *const result_vector, uint8_t picture_answer, const int vector_size)
{
    for (int i = 0; i < vector_size; i++)
    {
        result_vector[i] = 0.01;
    }

    result_vector[picture_answer] = 0.99;
}

void get_input_vector(double *const input_vector, uint8_t *start_input_vector, const int vector_size)
{
    for (int i = 1; i < vector_size + 1; i++)
    {
        input_vector[i - 1] = (start_input_vector[i] / 255.0 * 0.99) + 0.01;
    }
}

void train(network_t *const network, const data_t *const data)
{
    double *result = malloc(sizeof(double) * OUTPUT_NODES);
    double *input = malloc(sizeof(double) * INPUT_NODES);

    printf("%ld\n", data->inputed_size);

    for (int i = 0; i < data->inputed_size; i++)
    {
        get_result_vector(result, data->data[i][0], OUTPUT_NODES);
        get_input_vector(input, data->data[i], INPUT_NODES);
        train_by_picture(network, input, result);
    }

    free(result);
    free(input);
}