# Нейронная сеть для распознавания рукописных цифр

from numpy import*
from matplotlib.pyplot import*
from scipy.special import expit
import PIL
#from scipy.misc import imread
from scipy.ndimage.interpolation import rotate
import time

# Работа со своим изображением
'''
my_4 = matplotlib.pyplot.imread('../datasets/my.png')

input_plus_10 = rotate(my_4, 10, cval = 0.01, reshape = False)
        
input_minus_10 = rotate(my_4, -10, cval = 0.01, reshape = False)

show(imshow(my_4, cmap = 'Greys', interpolation = None))
show(imshow(input_plus_10, cmap = 'Greys', interpolation = None))
show(imshow(input_minus_10, cmap = 'Greys', interpolation = None))

my_4.resize(784)    
my_4 = ((255 - my_4) / 255.0 * 0.99) + 0.01
print(my_4)
'''

class Neural_Network():
    
    # Инициализация сети
    def __init__(self, inputnodes, hiddennodes, outputnodes, learningrate):
        
        # Кол-во узлов во входном, скрытом и выходном слоях
        self.inodes = inputnodes
        self.hnodes = hiddennodes
        self.onodes = outputnodes

        # Коэффициент обучения
        self.lr = learningrate

        # Весовые коэффициенты между слоями
        self.wih = random.normal(0.0, pow(self.hnodes, -0.5),
                                 (self.hnodes, self.inodes))
        self.who = random.normal(0.0, pow(self.onodes, -0.5),
                                 (self.onodes, self.hnodes))

        # Логистическая функция(функция активации)
        self.activation_function = lambda x: expit(x)
        
    # Тренировка сети на обучающей выборке
    def train(self, inputs_list, targets_list):
        
        # Получение матриц из одномерного списка
        inputs = array(inputs_list, ndmin = 2).T
        targets = array(targets_list, ndmin = 2).T

        # Прямое распространение сигналов
        hidden_inputs = dot(self.wih, inputs)
        hidden_outputs = self.activation_function(hidden_inputs)
        final_inputs = dot(self.who, hidden_outputs)
        final_outputs = self.activation_function(final_inputs)

        # Ошибки выходного слоя
        output_errors = targets - final_outputs
        # Ошибки скрытого слоя
        hidden_errors = dot(self.who.T, output_errors)
        

        self.who += self.lr * dot((output_errors * final_outputs * (1.0 - final_outputs)),
                                  transpose(hidden_outputs))

        self.wih += self.lr * dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)),
                                  transpose(inputs))
        '''    
        # Обновление весов между скрытым и выходным слоем
        delta_error_who = zeros((self.onodes, self.hnodes), dtype = float)
        for i in range(self.onodes):
            for j in range(self.hnodes):
                delta_error_who[i][j] = -(output_errors[i][0] * final_outputs[i][0] * (1 - final_outputs[i][0]) * hidden_outputs[j][0])
        
        self.who -= self.lr * delta_error_who
        
        # Обновление весов между скрытым и входным слоем
        delta_error_wih = zeros((self.hnodes, self.inodes), dtype = float)
        for i in range(self.hnodes):
            for j in range(self.inodes):
                delta_error_wih[i][j] = -(hidden_errors[i][0] * hidden_outputs[i][0] * (1 - hidden_outputs[i][0]) * inputs[j][0])
                
        self.wih -= self.lr * delta_error_wih
        '''
    # Опрос нейронной сети
    def query(self, inputs_list):
        # Преобразование списка в матрицу(ndarrsy)
        inputs = array(inputs_list, ndmin = 2).T

        # Входящие сигналы для скрытого слоя
        hidden_inputs = dot(self.wih, inputs)
        # Выходящие сигналы для скрытого слоя
        hidden_outputs = self.activation_function(hidden_inputs)
        # Входящие сигналы для выходного слоя
        final_inputs = dot(self.who, hidden_outputs)
        # Выходящие сигналы выходного слоя
        final_outputs = self.activation_function(final_inputs)
        
        return final_outputs

#def normilize():

start = time.time()
network = Neural_Network(784, 200, 10, 0.01)
data_file = open('../datasets/mnist_train.csv', 'r')
data_array = data_file.readlines()
data_file.close()

epochs = 1

for e in range(epochs):
    for i in range(len(data_array)):
    #for i in range(5000):    
        example = data_array[i].split(',')
        for j in range(len(example)):
            example[j] = int(example[j])
        ndexample = (array(example[1:], dtype = int) / 255.0 * 0.99) + 0.01
        example_output = zeros(10) + 0.01
        example_output[example[0]] = 0.99
        
        #input_plus_10 = rotate(ndexample.reshape(28, 28), 10, cval = 0.01, reshape = False)
        #input_plus_10.resize(784)
        #network.train(input_plus_10, example_output)
        
        #input_minus_10 = rotate(ndexample.reshape(28, 28), -10, cval = 0.01, reshape = False)
        #input_minus_10.resize(784)
        #network.train(input_minus_10, example_output)

        ndexample.resize(784)
        network.train(ndexample, example_output)
print("Train finished")
# Опрос сети
check_file = open('../datasets/mnist_test.csv', 'r')
check_array = check_file.readlines()
check_file.close()

ok = 0
total = len(check_array)
for i in range(len(check_array)):
    example = check_array[i].split(',')
    for j in range(len(example)):
        example[j] = int(example[j])
    ndexample = (array(example[1:], dtype = int) / 255.0 * 0.99) + 0.01
    output = network.query(ndexample)
    if (argmax(output) == example[0]):
        ok += 1
print(ok / total * 100, '% - efficiency', sep = '')

# print('My numbers')
# output = network.query(my_4)
# print(argmax(output))

#print(example[0])
#image_array = array(example[1:], dtype = int).reshape((28,28))
#show(imshow(image_array, cmap = 'Greys', interpolation = None))
end = time.time()
print('Time efficiency -', end - start)