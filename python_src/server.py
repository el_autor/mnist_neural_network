import requests
import time
import shutil
from PIL import Image
import os

url = "https://api.telegram.org/bot1018470962:AAE4BnFvLJtmMgk0monbCB4JhU0TmNVto8Q/"
file_url = "https://api.telegram.org/file/bot1018470962:AAE4BnFvLJtmMgk0monbCB4JhU0TmNVto8Q/"

class User:

    def __init__(self, user_id):
        self.id = user_id
        self.photo_waiting = False


def get_updates_json(url, offset):
    params = {'timeout': 10, 'offset' : offset}
    response = requests.get(url + 'getUpdates', data = params)
    return response.json()

def send_message(chat_id, text):
    params = {'chat_id': chat_id, 'text': text}
    response = requests.post(url + 'sendMessage', data = params)
    return response

def send_photo(chat_id, filename):
    file_object = open(filename, "rb")
    print("hello")
    params = {'chat_id' : chat_id}
    response = requests.post(url + 'sendPhoto', data = params, files = {'photo' : file_object})
    print(response.text)
    file_object.close()

# тут убрать try except
def get_chat_id(json_data):
    try:
        chat_id = json_data['message']['chat']['id']
    except KeyError:
        chat_id = json_data['edited_message']['chat']['id']

    return chat_id

def get_last_update(server_answer_json):    
    results = server_answer_json['result']

    if (len(results) != 0):
        total_updates = len(results) - 1
        return results[total_updates]

    return None

def add_new_user(users_objects_dict, new_id):
    users_objects_dict[new_id] = User(new_id)
    # print('in func ', users_objects_dict)

def id_is_new(users_objects_dict, new_id):
    if new_id in users_objects_dict.keys():
        return False

    return True

def get_file(url, file_url, file_id):
    params = {'file_id' : file_id}
    print('\n', file_id, '\n')
    response = requests.get(url + 'getFile', data = params) 
    json_data = response.json()
    filepath = json_data['result']['file_path']
    photo = requests.get(file_url + filepath, stream = True)
    loaded_file = open('./photos/file.jpg', 'wb')
    shutil.copyfileobj(photo.raw, loaded_file)
    loaded_file.close()

def get_content_type(message_data):
    possible_data = ['text', 'photo']

    for datatype in possible_data:
        if datatype in message_data.keys():
            return datatype

    return None 

def get_message_type(message_data):
    possible_types = ['message', 'edited_message']

    for message_type in possible_types:

        if message_type in message_data.keys():
            return message_type

    return None

def identify_number(photo_path):
    foto = Image.open(photo_path)
    foto = foto.convert('L')
    # foto.show()
    foto = foto.resize((28, 28))
    # foto.show()
    foto_data = list(foto.getdata())
    print(foto_data)


    for i in range(len(foto_data)):

        if (foto_data[i] > 180):
            foto_data[i] = 255

        foto_data[i] = 255 - foto_data[i]

    print(foto_data)

    example_file = open("./c_src/sources/bot_src/user_photo_data/user_photo_data.txt", 'w')
    example_file.write("0") # need to do this because of csv file format()
    
    for i in range(len(foto_data)):
        example_file.write(',' + str(foto_data[i]))

    example_file.close() 

    os.system("./get_result.sh")
    
    file_with_number = open("./c_src/sources/bot_src/user_photo_data/number.txt", 'r')
    recognized_number = int(file_with_number.readline())
    file_with_number.close()

    return recognized_number

def main():
    users_dict = dict()
    offset = None   

    while (True):
        new_updates = get_updates_json(url, offset)
        possible_commands = ['/start', '/recognize']
        updates_count = len(new_updates['result'])
        #print(new_updates['result'])

        if (updates_count != 0):
            last_update_id = new_updates['result'][updates_count - 1]['update_id']
            offset = last_update_id + 1
            
            for i in range(len(new_updates['result'])):
                message_type = get_message_type(new_updates['result'][i])
                current_id = new_updates['result'][i][message_type]['from']['id']
                is_new = id_is_new(users_dict, current_id)
                content_type = get_content_type(new_updates['result'][i][message_type])
                print('\n', content_type, '\n')

                if ((not is_new) and (users_dict[current_id].photo_waiting)):

                    if (content_type == 'photo'):
                        send_message(get_chat_id(new_updates['result'][i]), 'Right one!')
                        get_file(url, file_url, new_updates['result'][i]['message']['photo'][0]['file_id'])
                        send_message(get_chat_id(new_updates['result'][i]), 'Your number is ' + str(identify_number('./photos/file.jpg'))) 
                        users_dict[current_id].photo_waiting = False
                    else:
                        send_message(get_chat_id(new_updates['result'][i]), 'Its not a photo.')     

                elif ((content_type == 'text') and (new_updates['result'][i][message_type]['text'] == '/start') and (is_new)):
                    add_new_user(users_dict, current_id)
                    send_message(get_chat_id(new_updates['result'][i]), 'Hello!')

                elif ((content_type == 'text') and (new_updates['result'][i][message_type]['text'] == '/recognize') and (not is_new)):
                    send_message(get_chat_id(new_updates['result'][i]), 'Send me a photo with number in its centre.\n(jpg format only for now)')
                    send_photo(get_chat_id(new_updates['result'][i]), './photos/example.jpg')
                    send_message(get_chat_id(new_updates['result'][i]), 'Here is example.')
                    users_dict[current_id].photo_waiting = True

                elif (not is_new):

                    if (((content_type == 'text') and (new_updates['result'][i][message_type]['text'] not in possible_commands)) or (content_type == 'photo')):    
                        send_message(get_chat_id(new_updates['result'][i]), 'Invalid command! Look through valid ones!')
                    
            
        time.sleep(0.5)   

if __name__ == '__main__':
    main()

# nohup ... ps ax | grep 'runner.py' 