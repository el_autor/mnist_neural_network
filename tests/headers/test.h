#ifndef _TEST_H_

#define _TEST_H_

#include "../../c_src/headers/app.h"

// Possible errors in testing
#define DATA_MISMATCH 10

int test_result(FILE *const stream, const int errs_number, const int test_order);
int test_create(FILE *const stream);
int test_parse(FILE *const stream);
int test_transpose(FILE *const stream);

#endif