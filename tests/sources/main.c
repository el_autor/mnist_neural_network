#include "../headers/test.h"

static int analyze_result(FILE *const stream, const int errs_number, const char *const function_name)
{
    if (errs_number)
    {
        fprintf(stream, "Testing of %s function failed with %d errors!\n\n", function_name, errs_number);
    }
    else
    {
        fprintf(stream, "Testing of %s function passed.\n\n", function_name);
    }

    return errs_number;
}

int test_result(FILE *const stream, const int errs_number, const int test_order)
{
    if (errs_number)
    {
        fprintf(stream, "TEST %d FAILED!\n", test_order);
    }
    else
    {
        fprintf(stream, "TEST %d PASSED\n", test_order);
    }
    
    return errs_number;
}

int main()
{
    int errs_counter = 0;

    fprintf(stdout, "Testing create_network function\n");
    errs_counter += analyze_result(stdout, test_create(stdout), "create_network");

    fprintf(stdout, "Testing csv parser\n");
    errs_counter += analyze_result(stdout, test_parse(stdout), "parse_data");

    fprintf(stdout, "Test matrix operations\n");
    errs_counter += analyze_result(stdout, test_transpose(stdout), "transpose");

    if (errs_counter)
    {
        fprintf(stdout, "App testing failed with %d errors!\n", errs_counter);
    }
    else
    {
        fprintf(stdout, "App testing properly passed!\n");
    }

    return errs_counter;
}