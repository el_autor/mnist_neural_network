#include "../headers/test.h"

static int is_equal(FILE *const stream, const network_t *const network,\
                    const int in_nodes, const int hidden_nodes,\
                    const int out_nodes, const float learn_rate, double (*function)(double))
{
    if (network->i_nodes != in_nodes || network->h_nodes != hidden_nodes || network->o_nodes != out_nodes ||\
        network->learning_rate != learn_rate ||\
        network->activation_func != function)
    {
        fprintf(stream, "Static attributes mismatch!\n");
        return ERROR;
    }

    return OK;
}

static int check_static_values(FILE *const stream)
{
    network_t *network = NULL;
    
    if ((network = create_network(100, 200, 300, 0.1, &activation_function)) == NULL)
    {
        return ERROR;
    }

    if (is_equal(stream, network, 100, 200, 300, 0.1, &activation_function) != OK)
    {
        free_all(network);
        return ERROR;
    }

    free_all(network);
    return OK;
}

static int check_weights(FILE *const stream)
{
    double **weights = NULL;
    int cols = 5, rows = 5;
    double deviation = 1 / sqrt(rows);

    if ((weights = create_weights(rows, cols)) == NULL)
    {
        return ERROR;
    }

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            if (fabs(weights[i][j]) > deviation)
            {
                free_weights(weights, rows);
                return ERROR;
            }
        }
    }

    free_weights(weights, rows);
    return OK;
}

int test_create(FILE *const stream)
{
    int errs_counter = 0;

    //  Проверяем статические значения структуры
    fprintf(stream, "1)check static values\n");
    errs_counter += test_result(stream, check_static_values(stream), 1);

    // Проверяем матрицы весов
    fprintf(stream, "2)check weight matrices\n");
    errs_counter += test_result(stream, check_weights(stream), 2);

    return errs_counter;
}