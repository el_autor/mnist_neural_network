#include "../headers/test.h"

static int square_size = 10; 
static int rows_1 = 5, cols_1 = 10, rows_2 = 10, cols_2 = 3;

static double **create_matrix(const int rows, const int cols)
{
    double **matrix = NULL;

    matrix = malloc(sizeof(double *) * rows);

    for (int i = 0; i < rows; i++)
    {
        matrix[i] = malloc(sizeof(double) * cols);
    }

    return matrix;
}

static int matrix_transpose(const int rows, const int cols)
{
    double **matrix = NULL, **result_matrix = NULL, **transposed_matrix = NULL;

    matrix = create_matrix(rows, cols);

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            matrix[i][j] = i;
        }
    }

    result_matrix = create_matrix(cols, rows);

    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            result_matrix[i][j] = j;
        }
    }

    transposed_matrix = transpose(matrix, rows, cols);

    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            if (transposed_matrix[i][j] != result_matrix[i][j])
            {
                free_weights(matrix, rows);
                free_weights(result_matrix, cols);
                free_weights(transposed_matrix, cols);

                return ERROR;
            }
        }
    }

    free_weights(matrix, rows);
    free_weights(result_matrix, cols);
    free_weights(transposed_matrix, cols);

    return OK;
}

static int matrix_multiply(const int rows_1, const int cols_1, const int rows_2, const int cols_2)
{
    double **matrix_1 = NULL, **matrix_2 = NULL, **result_matrix = NULL, **proper_result = NULL;

    matrix_1 = create_matrix(rows_1, cols_1);
    matrix_2 = create_matrix(rows_2, cols_2);
    proper_result = create_matrix(rows_1, cols_2);

    for (int i = 0; i < rows_1; i++)
    {
        for (int j = 0; j < cols_1; j++)
        {
            matrix_1[i][j] = 1;
        }
    }

    for (int i = 0; i < rows_2; i++)
    {
        for (int j = 0; j < cols_2; j++)
        {
            matrix_2[i][j] = 1;
        }
    }

    for (int i = 0; i < rows_1; i++)
    {
        for (int j = 0; j < cols_2; j++)
        {
            proper_result[i][j] = rows_2;
        }
    }

    result_matrix = multiply(matrix_1, matrix_2, rows_1, cols_1, cols_2);

    for (int i = 0; i < rows_1; i++)
    {
        for (int j = 0; j < cols_2; j++)
        {
            if (result_matrix[i][j] != proper_result[i][j])
            {
                free_weights(matrix_1, rows_1);
                free_weights(matrix_2, rows_2);
                free_weights(proper_result, rows_1);
                free_weights(result_matrix, rows_1);

                return ERROR;
            }
        }
    }

    free_weights(matrix_1, rows_1);
    free_weights(matrix_2, rows_2);
    free_weights(proper_result, rows_1);
    free_weights(result_matrix, rows_1);

    return OK;
}

int test_transpose(FILE *const stream)
{
    int errs_counter = 0;

    fprintf(stream, "1)check square transpose\n");
    errs_counter += test_result(stream, matrix_transpose(square_size, square_size), 1);

    fprintf(stream, "2)check random transpose\n");
    errs_counter += test_result(stream, matrix_transpose(rows_1, cols_1), 2);

    
    fprintf(stream, "3)check square multiply\n");
    errs_counter += test_result(stream, matrix_multiply(square_size, square_size, square_size, square_size), 3);

    fprintf(stream, "4)check random multiply\n");
    errs_counter += test_result(stream, matrix_multiply(rows_1, cols_1, rows_2, cols_2), 4);

    return errs_counter;
}