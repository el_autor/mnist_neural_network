#include "../headers/test.h"

static int check_csv_10(FILE *stream)
{
    FILE *in_stream = NULL;
    data_t data = { NULL, 0, 0 };
    uint8_t pixel = 0;

    if (parse_data("../datasets/mnist_test_10.csv", &data) != OK)
    {
        return ERROR;
    }
    
    if ((in_stream = fopen("../datasets/mnist_test_10.csv", "r")) == NULL)
    {
        free_data(&data);
        return ERROR;
    }

    for (int i = 0; i < data.inputed_size; i++)
    {
        if (fscanf(in_stream, "%hhd", &pixel) != 1 || pixel != data.data[i][0])
        {
            free_data(&data);
            return ERROR;
        }

        for (int j = 1; j < INPUT_NODES + 1; j++)
        {
            if (fscanf(in_stream, ",%hhd", &pixel) != 1 || pixel != data.data[i][j])
            {
                printf("here4\n");
                free_data(&data);
                return ERROR;
            }
        }
    }

    fclose(in_stream);
    free_data(&data);
    return OK;
}

int test_parse(FILE *const stream)
{
    int errs_counter = 0;

    //  Парсинг файла с 10 фото
    fprintf(stream, "1)check csv file with 10 pics\n");
    errs_counter += test_result(stream, check_csv_10(stream), 1);

    return errs_counter;
}